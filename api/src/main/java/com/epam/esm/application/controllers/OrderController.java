package com.epam.esm.application.controllers;

import com.epam.esm.application.hateoas.LinkManager;
import com.epam.esm.repository.exception.OrderNotFoundException;
import com.epam.esm.service.OrderService;
import com.epam.esm.service.dto.OrderDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RestControllerAdvice(basePackages = "com.epam.esm.application.handler")
@RequestMapping("/api/order")
public class OrderController {

    private final OrderService orderService;

    @GetMapping()
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<List<OrderDTO>> getAll(@RequestParam(value = "page", required = false) Integer page,
                                                 @RequestParam(value = "size", required = false) Integer size) {
        List<OrderDTO> list;

        if (page == null || size == null)
            list = orderService.getAll();
        else
            list = orderService.findWithPagination(page, size);

        if (list.isEmpty()) throw new OrderNotFoundException();

        list.forEach(LinkManager::addLinks);

        return ResponseEntity.ok(list);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN' ) || hasAuthority('SCOPE_USER') and hasAuthority('SCOPE_ID_' + #id)")
    public ResponseEntity<OrderDTO> get(@PathVariable("id") Long id) {

        OrderDTO order = orderService.get(id);

        LinkManager.addLinks(order);

        return ResponseEntity.ok(order);
    }


}
