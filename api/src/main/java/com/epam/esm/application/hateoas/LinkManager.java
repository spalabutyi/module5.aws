package com.epam.esm.application.hateoas;

import com.epam.esm.application.controllers.CertificateController;
import com.epam.esm.application.controllers.OrderController;
import com.epam.esm.application.controllers.TagController;
import com.epam.esm.application.controllers.UserController;
import com.epam.esm.service.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.Link;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Slf4j
public class LinkManager {

    public static final String TARGET = "/api";
    public static final String REPLACEMENT = "";
    public static final String RELATION = "self";

    private LinkManager() {
    }

    public static void addLinks(CertificateDTO certificate) {
        if (certificate.hasLinks()) return;

        Link link = linkTo(methodOn(CertificateController.class).get(certificate.getId())).withSelfRel();

        certificate.add(trim(link));
        certificate.getTags().forEach(LinkManager::addLinks);
    }

    public static void addLinks(TagDTO tag) {
        if (tag.hasLinks()) return;

        Link link = linkTo(methodOn(TagController.class).get(tag.getId())).withSelfRel();

        tag.add(trim(link));
    }

    public static void addLinks(UserDTO user) {
        if (user.hasLinks()) return;

        Link link = linkTo(methodOn(UserController.class).get(user.getId())).withSelfRel();

        user.add(trim(link));
        user.getOrders().forEach(LinkManager::addLinks);
    }

    public static void addLinks(OrderDTO order) {
        if (order.hasLinks()) return;

        Link link = linkTo(methodOn(OrderController.class).get(order.getId())).withSelfRel();

        order.add(trim(link));
        addLinks(order.getCertificate());
        addLinks(order.getUser());
    }

    private static Link trim(Link link) {
        return Link.of(link.toUri().getPath().replace(TARGET, REPLACEMENT), RELATION);
    }

}