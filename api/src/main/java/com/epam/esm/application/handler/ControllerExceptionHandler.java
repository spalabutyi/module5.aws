package com.epam.esm.application.handler;

import com.epam.esm.exception.response.ErrorResponse;
import com.epam.esm.exception.response.Errors;
import com.epam.esm.exception.IncorrectCredentialException;
import com.epam.esm.exception.TokenException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.stream.Collectors;

@RestControllerAdvice
@PropertySource("classpath:errormessages.properties")
@RequiredArgsConstructor
public class ControllerExceptionHandler {

    private final Environment env;

    /*
     *Token exception
     */

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleTokenException(TokenException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(ex.getMessage(), Errors.BAD_REQUEST),
                HttpStatus.BAD_REQUEST);
    }

    /*
     * Common exceptions
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<ErrorResponse> handleMethodNotAllowedException(HttpRequestMethodNotSupportedException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        ex.getMessage(), Errors.NOT_ALLOWED), HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> handleValidationException(MethodArgumentNotValidException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        ex.getMessage(), Errors.BAD_REQUEST), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ErrorResponse> handleBadRequestException(MethodArgumentTypeMismatchException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        "Invalid input data", Errors.BAD_REQUEST), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IncorrectCredentialException.class)
    public ResponseEntity<ErrorResponse> handleIncorrectCredentialException(MethodArgumentTypeMismatchException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        ex.getMessage(), Errors.BAD_REQUEST), HttpStatus.BAD_REQUEST);
    }

    /**
     * Returns a concatenated string of validation error messages from the provided BindingResult object.
     *
     * @param result the BindingResult object containing field errors
     * @return a string containing error messages for each field error in the BindingResult object
     */

    public static String getErrorMessages(BindingResult result) {
        return result.getFieldErrors().stream()
                .map(e -> "wrong " + e.getField() + ": (" + e.getDefaultMessage() + ") ")
                .collect(Collectors.joining());
    }
}