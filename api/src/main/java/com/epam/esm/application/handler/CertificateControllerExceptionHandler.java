package com.epam.esm.application.handler;

import com.epam.esm.exception.response.ErrorResponse;
import com.epam.esm.exception.response.Errors;
import com.epam.esm.repository.exception.CertificateAddTagException;
import com.epam.esm.repository.exception.CertificateFailCreateException;
import com.epam.esm.repository.exception.CertificateFailUpdateException;
import com.epam.esm.repository.exception.CertificateNotFoundException;
import com.epam.esm.exception.ServiceException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@PropertySource("classpath:errormessages.properties")
@RequiredArgsConstructor
public class CertificateControllerExceptionHandler {

    private final Environment env;

    /*
     * Certificate exceptions
     */

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleCertificateNotFoundException(CertificateNotFoundException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(env.getProperty("certificate.not.found"), Errors.NOT_FOUND),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleCertificateFailCreateException(CertificateFailCreateException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        String.format(env.getProperty("certificate.creation.failure"), ex.getMessage()),
                        Errors.FAIL_CREATE), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleCertificateFailUpdateException(CertificateFailUpdateException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        String.format(env.getProperty("certificate.update.failure"), ex.getMessage()),
                        Errors.FAIL_CREATE), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleCertificateAddTagException(CertificateAddTagException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        ex.getMessage(), Errors.FAIL_CREATE), HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleServiceException(ServiceException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        ex.getMessage(), Errors.BAD_REQUEST), HttpStatus.BAD_REQUEST
        );
    }

}