package esm.service.impl;

import com.epam.esm.model.Certificate;
import com.epam.esm.model.Order;
import com.epam.esm.model.User;
import com.epam.esm.repository.CertificateRepository;
import com.epam.esm.repository.OrderRepository;
import com.epam.esm.repository.UserRepository;
import com.epam.esm.service.OrderService;
import com.epam.esm.service.dto.OrderDTO;
import com.epam.esm.service.impl.OrderServiceImpl;
import esm.config.MapperConfig;
import esm.config.TestConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@Profile("test")
@SpringBootTest(classes = {TestConfig.class, MapperConfig.class})
class OrderServiceImplTest {

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private CertificateRepository certificateRepository;

    @Autowired
    private ModelMapper modelMapper;

    private OrderService orderService;

    private Order testorder;

    private User testUser;
    private Certificate testCertificate;

    @BeforeEach
    void setUp() {
        orderService = new OrderServiceImpl(orderRepository, userRepository, certificateRepository, modelMapper);
        testUser = new User();
        testCertificate = new Certificate();
        testorder = new Order();
        testorder.setId(1L);
        testorder.setUser(testUser);
        testorder.setPrice(BigDecimal.ONE);
        testorder.setCertificate(testCertificate);
        testorder.setPurchaseDate(LocalDateTime.now());
    }

    @Test
    void getValidOrderSuccessTest() {

        when(orderRepository.findById(anyLong())).thenReturn(Optional.of(testorder));
        assertEquals(1L, orderService.get(1L).getId());
        verify(orderRepository).findById(1L);
    }

    @Test
    void getAllCertificatesSuccessTest() {
        List<Order> list = new ArrayList<>();
        list.add(testorder);

        when(orderRepository.findAll()).thenReturn(list);
        assertEquals(1, orderService.getAll().size());
        verify(orderRepository).findAll();
    }

    @Test
    void isExistsSuccessTest() {

        when(orderRepository.existsById(anyLong())).thenReturn(true);
        boolean idExists = orderService.isExists(1L);
        verify(orderRepository).existsById(anyLong());
        assertTrue(idExists);
    }

    @Test
    void addOrderSuccessTest() {

        when(userRepository.findById(anyLong())).thenReturn(Optional.of(testUser));
        when(certificateRepository.findById(anyLong())).thenReturn(Optional.of(testCertificate));
        when(orderRepository.findOrderByUserIdAndCertificateId(anyLong(), anyLong()))
                .thenReturn(Optional.empty());

        OrderDTO added = orderService.addOrder(2L, 2L);

        assertNotNull(added);

        verify(userRepository).findById(anyLong());
        verify(certificateRepository).findById(anyLong());
        verify(orderRepository).findOrderByUserIdAndCertificateId(anyLong(), anyLong());

    }

}