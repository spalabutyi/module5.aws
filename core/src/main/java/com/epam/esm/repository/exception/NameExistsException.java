package com.epam.esm.repository.exception;

public class NameExistsException extends RuntimeException{

    public NameExistsException(String message) {
        super(message);
    }
}
