package com.epam.esm.repository.exception;

public class UserOperationNotSupportedException extends RuntimeException{

    public UserOperationNotSupportedException(String message) {
        super(message);
    }

}
