package com.epam.esm.repository.exception;

public class CertificateFailCreateException extends RuntimeException{

    public CertificateFailCreateException(String message) {
        super(message);
    }
}
