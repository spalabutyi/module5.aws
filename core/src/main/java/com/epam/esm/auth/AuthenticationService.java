package com.epam.esm.auth;

import com.epam.esm.model.User;
import com.epam.esm.repository.UserRepository;
import com.epam.esm.repository.exception.UserFailCreateException;
import com.epam.esm.repository.exception.UserNotFoundException;
import com.epam.esm.security.JwtUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ControllerAdvice;

import static com.epam.esm.model.Role.USER;
import static com.epam.esm.service.impl.UserServiceImpl.USER_ALREADY_EXISTS;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
@ControllerAdvice(basePackages = "com.epam.esm.application.handler")
@Slf4j
public class AuthenticationService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtil jwtUtil;
    private final AuthenticationManager manager;


    @Transactional
    public AuthResponse register(RegisterRequest authUser) {

        if (userRepository.findByEmail(authUser.getEmail()).isPresent())
            throw new UserFailCreateException(String.format(
                    USER_ALREADY_EXISTS, authUser.getEmail()));

        var user = User.builder()
                .name(authUser.getName())
                .email(authUser.getEmail())
                .password(passwordEncoder.encode(authUser.getPassword()))
                .role(USER)
                .build();

        userRepository.save(user);

        log.info("New user [{}] registered", user);

        var jwtToken = jwtUtil.getToken(user);
        var refreshJwtToken = jwtUtil.getRefreshToken(user);
        return AuthResponse.builder()
                .userId(user.getId())
                .token(jwtToken)
                .refreshToken(refreshJwtToken)
                .issuedAt(jwtUtil.extractIssue(jwtToken))
                .expiresAt(jwtUtil.extractExpiration(jwtToken))
                .build();

    }

    public AuthResponse authenticate(AuthRequest dto) {
        manager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        dto.getEmail(),
                        dto.getPassword()
                )
        );

        var user = userRepository.findByEmail(dto.getEmail())
                .orElseThrow(() -> new UserNotFoundException("User not found"));


        log.info("[{}] authenticated", user);

        var jwtToken = jwtUtil.getToken(user);
        var refreshJwtToken = jwtUtil.getRefreshToken(user);
        return AuthResponse.builder()
                .userId(user.getId())
                .token(jwtToken)
                .refreshToken(refreshJwtToken)
                .issuedAt(jwtUtil.extractIssue(jwtToken))
                .expiresAt(jwtUtil.extractExpiration(jwtToken))
                .build();
    }

    public AuthResponse parseRefreshToken(String refreshToken) {
        String email = jwtUtil.extractUsername(refreshToken);
        var user = userRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException("User not found"));

        var jwtToken = jwtUtil.getToken(user);
        var refreshJwtToken = jwtUtil.getRefreshToken(user);
        return AuthResponse.builder()
                .userId(user.getId())
                .token(jwtToken)
                .refreshToken(refreshJwtToken)
                .issuedAt(jwtUtil.extractIssue(jwtToken))
                .expiresAt(jwtUtil.extractExpiration(jwtToken))
                .build();
    }

}