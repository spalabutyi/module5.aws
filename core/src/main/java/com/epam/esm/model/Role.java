package com.epam.esm.model;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Role {
    GUEST,
    USER,
    ADMIN;

}