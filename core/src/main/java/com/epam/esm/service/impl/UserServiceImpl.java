package com.epam.esm.service.impl;

import com.epam.esm.model.User;
import com.epam.esm.repository.UserRepository;
import com.epam.esm.repository.exception.UserNotFoundException;
import com.epam.esm.repository.exception.UserOperationNotSupportedException;
import com.epam.esm.service.UserService;
import com.epam.esm.service.dto.UserDTO;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

    public static final String USER_ALREADY_EXISTS = "The user with the email address '%s' already exists";
    public static final String UPDATE_NOT_SUPPORTED = "Unable to update user. Operation not supported";
    public static final String DELETE_NOT_SUPPORTED = "Unable to delete user. Operation not supported";
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    @Override
    public UserDTO add(UserDTO userDTO) {
        throw new UserOperationNotSupportedException(UPDATE_NOT_SUPPORTED);
    }

    @Override
    public UserDTO update(UserDTO userDTO) {
        throw new UserOperationNotSupportedException(UPDATE_NOT_SUPPORTED);
    }

    @Override
    public boolean delete(Long id) {
        throw new UserOperationNotSupportedException(DELETE_NOT_SUPPORTED);
    }

    @Override
    public UserDTO get(Long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) throw new UserNotFoundException();
        return modelMapper.map(user, UserDTO.class);
    }

    @Override
    public List<UserDTO> getAll() {
        return userRepository.findAll().stream()
                .map(user -> modelMapper.map(user, UserDTO.class))
                .toList();
    }

    @Override
    public boolean isExists(Long id) {
        return userRepository.existsById(id);
    }

    @Override
    public boolean isExists(String name) {
        return userRepository.findByName(name).isPresent();
    }

    @Override
    public List<UserDTO> findWithPagination(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        return userRepository.findAll(pageable).getContent().stream()
                .map(user -> modelMapper.map(user, UserDTO.class))
                .toList();
    }

    @Override
    public UserDTO getByLogin(String login) {
        Optional<User> user = userRepository.findByEmail(login);
        if (user.isEmpty())
            throw new UserNotFoundException();

        return modelMapper.map(user, UserDTO.class);
    }

}